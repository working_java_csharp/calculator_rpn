﻿using Domaine.Data;
using Metier.Model;
using Metier.Model.OperationVisitor;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using System.Collections.Concurrent;
//using System.Data.Entity;

namespace Metier.Manager
{
    public class CalculatorManager : ICalculatorManager
    {
        #region Fields
        private readonly CalculatorContext<CalculatorStack> _baseContext;
        private readonly IOpVisitor _opVisitor = ConcretOperation.Instance;
        private readonly object o = new object();
        #endregion Fields

        #region Constructor
        public CalculatorManager(CalculatorContext<CalculatorStack> baseContext)
        {
            _baseContext = baseContext; 
        } 
        #endregion Constructor

        #region Methods
            #region Create
        public async Task<CalculatorStack?> Create(CalculatorStack calculatorStack)
        {
            if(calculatorStack == null) return default;
            if (calculatorStack.Stack == null) calculatorStack.Stack = new List<Content>();
            return await _baseContext.Create(calculatorStack);
        }

        public async Task<CalculatorStack?> Create(int value)
        {
            var calculatorStack = new CalculatorStack() {
                Stack = new List<Content>()
                };
            lock (o) { calculatorStack.Stack.Add(new Content() { value = value}); }
            return await _baseContext.Create(calculatorStack);
        }

        public async Task<CalculatorStack?> Create(params int[] value)
        {
            if (value == null || value.Length == 0) return default; 
            var calculatorStack = new CalculatorStack()
            {
                Stack = new List<Content>()
            };
            Parallel.For(0, value.Length, i =>
            {
                lock(o) { calculatorStack.Stack.Add(new Content() { value = i}); }
            });
            return await _baseContext.Create(calculatorStack);
        }
        #endregion Create

            #region Operation
        public async Task<ConcurrentStack<int>> DoOperationAsync(string stackId, string operation)
        {
            var calculatorStack =  await FindByIdHelper(stackId);
            if(calculatorStack == null) throw new ArgumentException("stackId and operation not exist");
            if(calculatorStack.Stack == null || calculatorStack.Stack.Count <= 1) throw new ArgumentException("stack empty or contains just one element, you must add value before");
            calculatorStack.Stack = DoCalculStack(calculatorStack.Stack, operation);
            await _baseContext.Update(calculatorStack);
            return new ConcurrentStack<int>(calculatorStack.Stack.Select(x => x.value));
        }

        public async Task<List<int>> Push(string stackId, int val)
        {
            var calculatorStack = await FindByIdHelper(stackId);
            if (calculatorStack == null) throw new ArgumentException("stackId and operation not exist");
            if (calculatorStack.Stack == null) calculatorStack.Stack = new List<Content>();
            lock (o) { calculatorStack.Stack.Add(new Content() { value = val}); };
            await _baseContext.Update(calculatorStack);
            return new List<int>(calculatorStack.Stack.Select(x => x.value));
        }

        public async Task<List<int>> PushRange(string stackId, params int[] value)
        {
            var calculatorStack = await FindByIdHelper(stackId);
            if (calculatorStack == null) throw new ArgumentException("stackId and operation not exist");
            if (calculatorStack.Stack == null) calculatorStack.Stack = new List<Content>();
            Parallel.For(0, value.Length, i =>
            {
                lock(o) { calculatorStack.Stack.Add(new Content() { value = i}); }; 
            });
            await _baseContext.Update(calculatorStack);
            return new List<int>(calculatorStack.Stack.Select(x => x.value));
        }


        #endregion Operation

            #region Find
        public List<string> FindAllOp()=>_opVisitor.GetAllOperation();

        public async Task<List<Content>> FindStackContent(string id)
        {
            var calculatorStack = await FindByIdHelper(id);
            if (calculatorStack == null) throw new ArgumentException("stackId and operation not exist");
            if (calculatorStack.Stack == null) calculatorStack.Stack = new List<Content>();
            return new List<Content>(calculatorStack.Stack);
        } 

        public async Task<CalculatorStack?> FindStackById(string stackId)
        {
            if(String.IsNullOrWhiteSpace(stackId)) throw new ArgumentException("stackId can not be null or white space");
            return await FindByIdHelper(stackId);
          
        }

        public IEnumerable<CalculatorStack> FindStacks()
        {
            return _baseContext.FindAll();
        }

             #endregion Find

            #region Delete
        public async Task<bool> Delete(string stackId)
        {
            if(String.IsNullOrWhiteSpace(stackId)) throw new ArgumentException("stackId can not be null or white space");
            var calcStack = await FindByIdHelper(stackId);
            if (calcStack == null) throw new ArgumentException("stack with this id not exist");
            return await _baseContext.Delete(calcStack) > 0;
        }
        #endregion Delete

            #region Helper
        private bool CheckCalcStack(CalculatorStack calculatorStack)
        {
            if (calculatorStack == null) throw new ArgumentException("stackId and operation not exist");
            if (calculatorStack.Stack == null) throw new ArgumentException("stack empty or contains just one element, you must add value before");
            return true;
        }

        private List<Content> DoCalculStack(ICollection<Content> ints, string operation)
        {
            var stack = new ConcurrentStack<Content>(ints.ToList());
            if (!stack.TryPop(out Content right) || !stack.TryPop(out Content left)) throw new Exception("Exception when try to get value in stack");
            stack.Push(new Content() { value = _opVisitor.DoOperation(operation, left.value, right.value) });
            return stack.ToList();
        }


        private async Task<CalculatorStack?> FindByIdHelper(string stackId)
        {
            if (stackId == null) throw new ArgumentNullException(stackId);
            if (_baseContext.Base == null || _baseContext.Base.Count() == 0) throw new Exception("Data Collection empty on DataBase");
            return await _baseContext.Base.Include(x => x.Stack)
                             .FirstOrDefaultAsync(x => x.Id == stackId);
        }



        #endregion Helper

        #endregion Methods



    }
}
