﻿using Metier.Model.OperationVisitor;

namespace Metier.Model.Operation
{
    public class Division : AbstractOperation
    {
        public Division(int left, int right) : base(left, right)
        {
        }

        public override string Name => nameof(Division);

        public override string Description => "Division between two int";

        public override int DoPeration()
        {
            if(Right == 0) throw new ArgumentException("the divisor must be greater than 0");
            return Left / Right;
        }

        public override int VisitOperation(IOpVisitor visitorOp)=>visitorOp.VisiteOp(this);

        public override string ToString()
        {
            return $"{Name} : /,Div";
        }
    }
}
