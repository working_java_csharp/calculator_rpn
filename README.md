
# Calculator_RPN

## Data Base Access
The project uses SQLITE, so you can install SQLite.
The connection string is in the appsetting file and the database name is LocalDatabase.db at the root of the solution.

## Getting started

To make it easy for you to get started this project, here's a list of recommended next steps.


## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/Brice_Zokpato/calculator_rpn.git
git branch -M main
git push -uf origin main
```


## Start and Test

- Open Solution 
- Start Project 'Api'
- Postman page will open in your broswer and you can test API
  ## You can also test with front angular client
    - Go to the folder 'ClientApp'
    - Open console
    - Run 'ng start' or 'ng serve'




***


## Name
Four project :
- Domaine :  Database access and object
- Metier :  business part and management rules
- API 
- ClientApp : Angular client

## Description
- Objective: Implementation of an RPN (reverse Polish notation) calculator in client/server mode.
- Languages:
- Backend: REST API, .Net core
- Frontend : Swagger
- Requested features:
                    Adding an element to a stack
                    Stack retrieval
                    Stack cleaning
- operation
            Operation -
            Operation *
            Operation /

Principle of operation of an RPN calculator:

Stack : []
Push de 10
Stack : [ 10 ]
Push de 5
Stack : [ 10, 5 ]
Push de 6
Stack : [ 10, 5, 6 ]
Operand ‘+’
Stack : [ 10, 11 ]


## Installation
.Net development environment
Visual Studio 2020 and more version


## Authors
Brice Ibo Zokpato
briceibo.com

