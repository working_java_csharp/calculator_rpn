﻿using Microsoft.AspNetCore.Mvc;
using Metier.Manager;
using Metier.Model;

namespace Calculator.Controllers
{
    [Route("[controller]")]
    public class RpnController : Controller
    {
        private readonly ICalculatorManager _calculatorManager;
        public RpnController(ICalculatorManager calculatorManager)
        {
            _calculatorManager = calculatorManager;
        }

        #region GET

        /// <summary>
        /// Retrieve list all operand
        /// </summary>
        [HttpGet("/op")]
        public  IActionResult GetAllOperation()
        {
            var allOperation = _calculatorManager.FindAllOp();
            if (allOperation == null || !allOperation.Any())
                return NotFound();
            return Ok(allOperation);
        }

        /// <summary>
        /// Retrieve stack by id
        /// </summary>
        [HttpGet("/stack/{stackId}")]
        public async Task<IActionResult> GetStackById(string stackId)
        {
            try
            {
                var result = await _calculatorManager.FindStackById(stackId);
                if(result == null) return NotFound();
                return Ok(result);
            } catch(Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        
        }

        [HttpGet("/stacks/{stackId}")]
        public async Task<IActionResult> GetStackContent(string stackId)
        {
            try
            {
                var result = await _calculatorManager.FindStackContent(stackId);
                if (result == null) return NotFound();
                return Ok(result);
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        /// <summary>
        /// Find all stack
        /// </summary>
        [HttpGet("/stacks")]
        public IActionResult FindAllStack()
        {
            var result =   _calculatorManager.FindStacks();
            //if (result == null) return NotFound();
            return Ok(result);
        }
        #endregion GET

        #region DELETE
        /// <summary>
        /// Delete Stack by id
        /// </summary>
        [HttpDelete("/stack/{stack_id}")]
        public async Task<IActionResult> Delete(string stack_id)
        {
            try
            {
                var result = await _calculatorManager.Delete(stack_id);
                return Ok(result);
            } catch(Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
            
        }

        #endregion DELETE

        /// <summary>
        /// Create new stack
        /// </summary>
        #region POST
        [HttpPost("/stack")]
        public async Task<IActionResult> CreateStack()
        {
            try
            {
                var result = await _calculatorManager.Create(new CalculatorStack() { Stack = new List<Content>() });
                if(result == null) return NotFound();
                return Ok(result);
            }
            catch(Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
            
        }

        /// <summary>
        /// Do operaand 'op' on stack with id 'stack_id'
        /// </summary>
        [HttpPost("/op/{op}/stack/{stack_id}")]
        public async Task<IActionResult> DoOperation(string op, string stack_id)
        {
            try
            {
                var result =  await _calculatorManager.DoOperationAsync(stack_id, op);
                return Ok(result);
            }catch(Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
           
        }

        /// <summary>
        /// Add new value 'value' on stack with id 'stack_id'
        /// </summary>
        [HttpPost("/stack/{stack_id}/{value}")]
        public async Task<IActionResult> AdValue(int value, string stack_id)
        {
            try
            {
                var result =  await _calculatorManager.Push(stack_id, value);
                return Ok(result);
            }catch(Exception e)
            {
                return new BadRequestObjectResult(e.Message);

            }
            
        }
        #endregion POST
    }
}
