﻿using Domaine.Data;
using Metier.Manager;
using Metier.Model;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;

namespace Calculator.Services
{
    public static class ConfigService
    {
        public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration config)
        {
            return services.AddDbContext<CalculatorContext<CalculatorStack>>(options => {
                options.UseSqlite(config.GetConnectionString("CalculatorDBContext") ?? throw new InvalidOperationException("Connection string 'CalculatorContext' not found."));
               // options.EnableRetryOnFailure();

            });

        }

        public static IServiceCollection AddDependencyGroup(this IServiceCollection services)
        {
            services.AddScoped<IBaseContext<CalculatorStack>, CalculatorContext<CalculatorStack>>();
            services.AddScoped<CalculatorContext<CalculatorStack>>();
            //services.AddScoped<IOpVisitor, ConcretOperation>();
            services.AddScoped<ICalculatorManager, CalculatorManager>();

            return services;
        }
    }
}
